# Overview
- [What is React (My blog post)?](./docs/react-intro.md)
- React is DOM management and user event library
- React is a library, not a framework
- React is one-way data binding (vs two-way)
- The Component is the fundamental building block in a React application:
    - Properties (props):
        - React Components re-render when props change
        - Props are passed down from parent React Components to child Components
        - Props are READ ONLY
        - Callbacks are used to send data back to the parent React Component
    - State: 
       - React Components re-render when state changes
       - State is private data accessible only within the React Component
       - State is created by the hook: `useState`
       - IMPORTANT: A state change occurs when the reference to the state variable changes (e.g. Adding items to an array or dictionary is not a "reference" change)
- Fun fact: The React Component was considered architectually "poor" because of the lack of separation between the UI and business logic.

# React Virtual DOM      
We should avoid manipulating the real DOM in our React applications because React
uses a virtual DOM.  React syncs the virtual DOM with the real DOM.  If we maniplulate the real DOM in a React application, we could confuse the React DOM synchronization logic.

![](./docs/react-overview.png)

