import './App.css';
import Type1 from './comps/Type1';
import Type2 from './comps/Type2';
import Type3 from './comps/Type3';
import Type4 from './comps/Type4';
import Type5 from './comps/Type5';
import Type6 from './comps/Type6';
import Type7 from './comps/Type7';
import Type8 from './comps/Type8';
import Type9 from './comps/Type9';
import Type10 from './comps/Type10';
import Type11 from './comps/Type11';

function App() {
  return (
    <div className="App">
      <Type1 />
      <Type2 message="Hello World!!"/>
      <Type3>
        <Type1 />
        <Type2 message="Hello World!!"/>
      </Type3>
      <Type4 />
      <Type5 initialValue={5150}/>
      <Type6 initialValue={5150} />
      <Type7/>
      <Type8 userId={1}/>
      <Type9 />
      <Type10 />
      <Type11 />
    </div>
  );
}

export default App;
