import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render( // <== Kicks off the reconciliation process (generate virtual DOM and insert it into the real DOM)
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

//
// Essentially looks like this: 
//   React.render(ourRootComponent, rootDOMElement)
//