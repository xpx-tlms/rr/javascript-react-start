import React from 'react';
import { useState } from 'react';

import './Type5.scss';

const Type5 = (props) => {
    let [counter, setCounter] = useState(props.initialValue); // <== Setting state from properties.

    const onClick = () => {
        setCounter(++counter);
    }

    return (
        <div className="Type5">
            <button onClick={() => { onClick() }}>Push</button>
            <span>{counter}</span>
        </div>
    )
}

export default Type5;