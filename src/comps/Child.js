import React from 'react'

export const Child = (props) => {
    return (
        <div className='Child'>
            <button onClick={() => props.onHandleNumber(props.number * 2)}>Push</button>
        </div>
    )
}
