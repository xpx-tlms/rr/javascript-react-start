import React from 'react';
import { useState, useEffect } from 'react';
import axios from 'axios'; // <== Note: No curly braces here.

import './Type8.scss';

const Type8 = (props) => {
    let [todoList, setTodoList] = useState([]);

    useEffect(() => {
        const fetch = async () => {
            const res = (await axios.get(`https://jsonplaceholder.typicode.com/todos?userId=${props.userId}`)).data;
            setTodoList(res);
        }
        fetch();
    }, [props.userId]); 

    //
    // IMPORTANT!!!! 
    // ALWAYS keep the Network tab open in the DevTools and select Fetch/XHR to make sure you are
    // not recursively calling the API!  XHR stands for XmlHttpRequest which is a library
    // that ships with the browser which makes async API calls.  It was built for XML SOAP 
    // requests, but it works fine for JSON requests.
    //

    return (
        <div className='Type8'>
            <table>
                <tbody>
                    { 
                        todoList.map(i => 
                            <tr key={i.title}><td>ID: {i.id} TODO: {i.title}</td></tr>
                        )
                    }
                </tbody>
            </table>
            {todoList.title}
        </div>
    )
}

export default Type8;