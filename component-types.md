# Component Types
These are some common React Component types.  These names are NOT official names, they are here to facilitate the learning of React Components. NOTE: This is NOT a complete list of Component types.  Attempting to create such a list is similar to attempting to group all the types of paintings in the world.

The Components listed below are used in [App.js](./src/App.js).  They reside in the `comps` folder.

|Name                            |Description              |Props| useState  | useRef | useEffect |
|--------------------------------|-------------------------|-----|-----------|--------|-----------|
|[Type 1](./src/comps/Type1.js)  |Simple                   |     |           |        |           |
|[Type 2](./src/comps/Type2.js)  |Property                 |X    |           |        |           |
|[Type 3](./src/comps/Type3.js)  |Component wrapper        |X    |           |        |           |
|[Type 4](./src/comps/Type4.js)  |Increment Counter        |     |X          |        |           |
|[Type 5](./src/comps/Type5.js)  |Increment Counter        |X    |X          |        |           |
|[Type 6](./src/comps/Type6.js)  |Inc/Reset Counter        |X    |X          |X (both)|           |
|[Type 7](./src/comps/Type7.js)  |Call API                 |     |X          |        |X          |
|[Type 8](./src/comps/Type8.js)  |Call API w/map()         |X    |X          |        |X          |
|[Type 9](./src/comps/Type9.js)  |Exchange Data            |X    |X          |        |           |
|[Type 10](./src/comps/Type10.js)|Array as State           |     |X          |X       |X          |
|[Type 11](./src/comps/Type11.js)|Show/Hide Enable/Disable |     |X          |        |           |

# Components
![](./docs/component-types.png)
