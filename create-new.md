# Creating a New React Project
- Execute: `npx create-react-app my-app`
- This creates a fully functional React application called my-app
- Unfortunately your new React project comes with stuff we don't need
- Stop the live server: `Ctl+c`
- Replace `App.js` with this:

```
import './App.css';

function App() {
  return (
    <div className="App">
      <div>Hello World!</div>
    </div>
  );
}

```

- Replace `App.css` with this: 
```
* {
  margin: 0;
  padding: 0;
}
```

- Start the live server: `npm start`
- You should see a minimalistic web page that looks like this:

![](/docs/start.png)